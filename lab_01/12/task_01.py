import sys

'''
Обчислити висоту циліндра, якщо задано його об’єм та площа основи.
'''


def main():

    volume = conversion_to_float(input("Введіть об'єм циліндра: "))
    base_area = conversion_to_float(input("Введіть площу основи циліндра: "))
    check_area_more_0(base_area)
    h = volume / base_area
    value_not_isinstance_float(h)
    print(f"Висота циліндра: {h}")
    sys.exit(0)

def conversion_to_float(value):
    try:
        value = float(value)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для об'єму та площі основи циліндра.")
        sys.exit(1)
    return value

def check_area_more_0(value):
    if value <= 0:
        print("Некоректні дані. Площа основи циліндра повинна бути додатньою.")
        sys.exit(1)
def value_not_isinstance_float(value):
    if not isinstance(value, float):
        print(value)
        sys.exit(1)

if __name__ == "__main__":
    main()
