import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (cos(x - y))^2 - (cos(11/8*x + y))^2
Z2 = sin(x/2)
Z3 = Z1/y - x/Z2
'''

def main():

    x = conversion_to_float(input("Введіть значення x: "))
    y = conversion_to_float(input("Введіть значення y: "))
    check_equality_0(y)
    z1_part1 = math.cos(x - y)
    z1_part2 = math.cos((11 / 8) * x + y)
    z1 = (z1_part1) ** 2 - (z1_part2) ** 2
    z2 = math.sin(x / 2)
    z3 = (z1 / y - x / z2)
    print(f"Z1 = {z1}")
    print(f"Z2 = {z2}")
    print(f"Z3 = {z3}")
    sys.exit(0)

def check_equality_0(value):
    if value == 0:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
def conversion_to_float(value):
    try:
        value = float(value)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)
    return value

if __name__ == "__main__":
    main()
