import unittest
from unittest.mock import patch
from io import StringIO
import sys
from task_01 import main

class TestTask_01(unittest.TestCase):

    def string_console_result(self, console_output):
        string_to_remove_volume = "Введіть об'єм циліндра: "
        string_to_remove_area = "Введіть площу основи циліндра: "
        result_string = console_output.replace(string_to_remove_volume, "").replace(string_to_remove_area, "").replace(
            "  ", " ")
        return result_string

    @patch('sys.stdin', StringIO('6\n3\n'))
    @patch('sys.stdout', new_callable = StringIO)
    def test_division_6by3_2returned(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        actual_Output = self.string_console_result(stdout.getvalue())
        expected_Output = f"Висота циліндра: {2.0}\n"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO('6\n0\n'))
    @patch('sys.stdout', new_callable = StringIO)
    def test_division_6by0_error_returned(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        actual_Output = self.string_console_result(stdout.getvalue())
        expected_Output = f"Некоректні дані. Площа основи циліндра повинна бути додатньою.\n"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO('A\n3\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_division_Aby3_FormatException_returned(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        actual_Output = self.string_console_result(stdout.getvalue())
        expected_Output = f"Некоректні дані. Введіть числові значення для об'єму та площі основи циліндра.\n"
        self.assertEqual(actual_Output, expected_Output)

    @patch('sys.stdin', StringIO(f"{sys.float_info.max + sys.float_info.max}\n1\n"))
    @patch('sys.stdout', new_callable=StringIO)
    def test_division_MaxValue_by1_OverflowException_returned(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        actual_Output = self.string_console_result(stdout.getvalue())
        expected_Output =expected_Output = f"Висота циліндра: inf\n"
        self.assertEqual(actual_Output, expected_Output)

if __name__ == '__main__':
    unittest.main()

