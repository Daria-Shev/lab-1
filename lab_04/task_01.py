import sys

'''
Обчислити висоту циліндра, якщо задано його об’єм та площа основи.
'''

def main():
    сalculat = CalculationСylinder()
    сalculat.input_value()
    сalculat.check_area_more_0()
    сalculat.search_h()
    sys.exit(0)

class CalculationСylinder:
    volume = None
    base_area = None

    def input_value(self):
        self.volume = self.conversion_to_float(input("Введіть об'єм циліндра: "))
        self.base_area = self.conversion_to_float(input("Введіть площу основи циліндра: "))

    def conversion_to_float(self, value):
        try:
            value = float(value)
        except ValueError:
            print("Некоректні дані. Введіть числові значення для об'єму та площі основи циліндра.")
            sys.exit(1)
        return value

    def check_area_more_0(self):
        if self.base_area <= 0:
            print("Некоректні дані. Площа основи циліндра повинна бути додатньою.")
            sys.exit(1)

    def search_h(self):
        h = self.volume / self.base_area
        self.value_not_isinstance_float(h)
        print(f"Висота циліндра: {h}")

    def value_not_isinstance_float(self, value):
        if not isinstance(value, float):
            print(value)
            sys.exit(1)

if __name__ == "__main__":
    main()
