import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (cos(x - y))^2 / (cos(11/8*x + y))^2
Z2 = sin(x / (2 - y))
Z3 = Z1/y - x/Z2
Заміна алгоритму(видалення перевірки бо cos приближується до нуля, але не рівний нулю)
Заміна виключення перевіркою умови
Відокремлення методу
Заміна змінної викликом методу

Запахи
Довгий метод
'''

def main():
    сalculat = Calculation()
    сalculat.input_value()
    сalculat.search_z()
    sys.exit(0)

class Calculation:
    x = None
    y = None

    def input_value(self):
        self.x = self.conversion_to_float(input("Введіть значення x: "))
        self.y = self.conversion_to_float(input("Введіть значення y: "))

    def conversion_to_float(self, value):
        if not (value.replace(".", "", 1).isdigit()):
            print("Некоректні дані. Введіть числові значення для x та y.")
            sys.exit(1)
        value = float(value)
        return value
    def conditions_exit(self):
        if (2 - self.y) == 0:
            return True
        if self.y == 0:
            return True
        return False
    def search_z(self):
        if (self.conditions_exit()):
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
        z1_part1 = math.cos(self.x - self.y) ** 2
        z1_part2 = math.cos((11 / 8) * self.x + self.y) ** 2
        print(f"Z1 = {z1_part1 / z1_part2}")
        print(f"Z2 = {math.sin(self.x / (2 - self.y))}")
        print(f"Z3 = {(z1_part1 - z1_part2) / self.y - self.x / (math.sin(self.x / 2))}")


if __name__ == "__main__":
    main()
